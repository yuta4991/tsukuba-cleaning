# README #

このデータはGullpを導入する必要があります。


## Step1

グローバル環境にGulpがない場合はインストールしてください。

```bash
$ npm install -g gulp
```

次にローカル環境にインストールします。  
※この`README.md`と同階層にインストールします。

```bash
$ npm install
```

## Step2

実際にGulpを動かします。  
ターミナルで`README.md`と同階層に移動したら以下コマンドを実行してください。

```bash
$ gulp
```

ブラウザが立ち上がりページが開いたら正常に動作されました。  

## Memo

- 作業は`/src/`で行います
- URL`local:3000/styleguide/`でスタイルガイドを確認できます

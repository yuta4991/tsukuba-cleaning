/**
 * 本ファイルまでの相対パス
 */
var PATH = (function () {
	var scripts = document.getElementsByTagName('script');
	var src = scripts[scripts.length - 1].getAttribute('src');
	var index = src.lastIndexOf('/');
	return (index !== -1) ? src.substring(0, index) : '';
})();

/**
 * ブレークポイント
 */
var BREAKPOINT = 640;

/**
 * IE判別
 */

var ua = navigator.userAgent;
var isIE = ua.match(/msie/i),
	isIE8 = ua.match(/msie [8.]/i),
	isIE9 = ua.match(/msie [9.]/i),
	isIE10 = ua.match(/msie [10.]/i);
	isIE11 = ua.match(/msie [11.]/i);
if (isIE) {
	$("body").addClass('ie');
	if (isIE8) {
		$("body").addClass('ie8');
	} else if (isIE9) {
		$("body").addClass('ie9');
	} else if (isIE10) {
		$("body").addClass('ie10');
	} else if (isIE11) {
		$("body").addClass('ie11');
	}
}


/**
 * ページ内のスムーズスクロール
 *
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v1.7.2
 * @require jQuery Easing v1.3
 */
function setSmoothScroll($) {
	$('a[href^="#"], area[href^="#"]').not('a[href="#"], area[href="#"]').each(function() {
		var hash = $(this).attr('href');
		this.data = {
			hash: hash,
			href: (function() {
				var a = document.createElement('a');
				a.href = hash;
				return a.href;
			})()
		};
	}).on('click', function() {
		var data = this.data;
		var $target = $(data.hash);
		if($target.length !== 0) {
			$('html, body').stop(true).animate(
				{ scrollTop: $target.offset().top +'px' },
				1000,
				'easeOutQuart',
				function() { location.href = data.href; }
			);
		}
	});
}

/**
 * リンクの別ウィンドウ表示
 *
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v1.7.2
 */
function setLinkNewWindow($) {
	var $targets = $('a[href^="http://"], a[href^="https://"]').not('a[href^="http://'+ location.hostname +'"], a[href^="https://'+ location.hostname +'"], a[data-rel="external"]');
	$targets.on('click', function() {
		open($(this).attr('href'), null);
		return false;
	});
}

/**
 * nav ボタン
 *
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v1.7.2
 */
$(function() {
			  $("#panel-btn").click(function() {
			    $("#panel").slideToggle(400);
			    $("#panel-btn-icon").toggleClass("close");
			    return false;
			  });
			});


/**
 * header要素のリロード時フェードイン
 *
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v1.7.2
 */
$(document).ready(function() {
			$('.header__logo').fadeIn(2000);
			$('.header__item--text').fadeIn(3000);
			$('.header__item--box').fadeIn(5000);
			$('.ceo__son').fadeIn(6000);
			});

/**
 * スロースクロール
 *
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v1.7.2
 */
$(document).ready(function() {
 $('#fullpage').fullpage({
 	scrollingSpeed: '1000'
 });
});



/**
 * slick.js
 *
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v1.7.2
 */
$(document).ready(function(){
  $('.your-class').slick({
    オプション: 値
  });
});



/**
 * 読み込み完了時の処理
 *
 * @require jQuery v1.7.2
 */
jQuery(function($) {
	setSmoothScroll($);
	setLinkNewWindow($);
});
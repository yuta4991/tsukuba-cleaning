/* =====================================================================
    グローバル変数
 * ================================================================== */
var $document = $(document);
var $window = $(window);
var $BREAKPOINT = 768;


/* =====================================================================
    プログラム
 * ================================================================== */
/**
 * @description UserAgent
 * スマホかタブレットか判定する
 */
var _ua = (function(u) {
    return {
        Tablet:u.indexOf("ipad") != -1 ||
            (u.indexOf("android") != -1 && u.indexOf("mobile") == -1) ||
            (u.indexOf("firefox") != -1 && u.indexOf("tablet") != -1) ||
            u.indexOf("kindle") != -1 ||
            u.indexOf("silk") != -1 ||
            u.indexOf("playbook") != -1,

        Mobile:(u.indexOf("windows") != -1 && u.indexOf("phone") != -1) ||
            u.indexOf("iphone") != -1 ||
            u.indexOf("ipod") != -1 ||
            (u.indexOf("android") != -1 && u.indexOf("mobile") != -1) ||
            (u.indexOf("firefox") != -1 && u.indexOf("mobile") != -1) ||
            u.indexOf("blackberry") != -1
    };
})(window.navigator.userAgent.toLowerCase());

/**
 * @description UserAgent
 * ブランザ判定
 */
var _browser = (function(b) {
    return {
        IE:         b.indexOf('msie') != -1 || b.indexOf('trident') != -1,
        edge:       b.indexOf('edge') != -1,
        chrome:     b.indexOf('chrome') != -1,
        safari:     b.indexOf('safari') != -1,
        firefox:    b.indexOf('firefox') != -1,
        opera:      b.indexOf('opera') != -1
    };
})(window.navigator.userAgent.toLowerCase());

/**
 * @description 便利な関数群
 * @type {Object}
 */
var utils = {
    getRandomInt: function(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },
    escapeHtml: function(string) {
        if(typeof string !== 'string') {
            return string;
        }
        return string.replace(/[&'`"<>]/g, function(match) {
            return {
                '&': '&amp;',
                "'": '&#x27;',
                '`': '&#x60;',
                '"': '&quot;',
                '<': '&lt;',
                '>': '&gt;',
            }[match];
        });
    },
    splitString: function(stringToSplit,separator) {
        var arrayOfStrings = stringToSplit.split(separator);
        return arrayOfStrings;
    }
};

/**
 * @description aタグにセットするスクリプト
 * @type {Object}
 */
var atags = {
    /**
     * @description スムーズスクロール
     * 
     */
    smoothScroll: function() {
        $(function(){
            $('a[href^="#"]').click(function() {
                var speed = 400;
                var href= $(this).attr("href");
                var target = $(href == "#" || href == "" ? 'html' : href);
                var position = target.offset().top - 100 + 'px';
                $('body,html').animate({scrollTop:position}, speed, 'swing');
                return false;
            });
        });
    },

    /**
     * @description 言語切り替えボタン
     * 
     */
    languageChange: function() {
        var topBtn = $('.l-footer__language');
        topBtn.hide();
        //スクロールが100に達したらボタン表示
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
            //ボタンの表示方法
                topBtn.fadeIn();
            } else {
            //ボタンの非表示方法
                topBtn.fadeOut();
            }
        });
    }
};


/**
 * HEADER 
 * 
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v3.1.1
 */
var header = {

    windowWidth: $window.width(),
    header: $('.l-header'),
    headerButton: $('.l-header__btn'),
    headerGnav: $('.l-header__gnav'),
    headerGnavItem: $('.l-nav__ul__item'),
    pulldown: $('.l-pulldown'),
    pulldownUl: $('.l-pulldown__ul'),
    pulldownItem: $('.l-pulldown__ul__item'),
    nav: $('.l-nav'),
    login: $('.l-header__laungage'),
    BREAKPOINT: 768,

    /**
     * @description Header Navigation
     * SP時の動き
     */
    navigation: function () {
        var t = this;

        t.headerButton.on('click', function() {

            if(t.headerButton.hasClass('js-active')) {
                $('body').css({ "overflow": "visible", "height": "auto" });

                t.headerGnav.slideUp();
                t.headerButton.removeClass('js-active');
                t.headerGnav.removeClass('js-active');
            } else {

                $('body').css({ "overflow": "hidden", "height": "100%" });

                t.headerGnav.slideDown();
                t.headerButton.addClass('js-active');
                t.headerGnav.addClass('js-active');
            }
        });

        // liをクリックした時の処理
        $('a[data-page]').each(function() {
            $(this).on('click touch', function() {
                t.headerGnav.slideUp();
                t.headerButton.removeClass('js-active');
                t.headerGnav.removeClass('js-active');
                $('body').css({ "overflow": "visible", "height": "auto" });
            });
        });
    },

    innerPulldown: function() {
        var t = this;

        if(t.windowWidth < t.BREAKPOINT) {
            t.pulldown.each(function() {
                $(this).on('click', function() {
                    if($(this).hasClass('js-active')) {
                        $(this).find('.l-pulldown__ul').slideUp();
                        $(this).removeClass('js-active');
                    } else {
                        $(this).find('.l-pulldown__ul').slideDown();
                        $(this).addClass('js-active');
                    }
                });
            })
        }
    },

    changeHeaderDOM: function() {
        var t = this;

        // 子要素の幅を取得
        if(t.windowWidth > t.BREAKPOINT) {
            // 子要素の幅200px
            t.login.after(t.nav);
        }
    },

    init: function() {
        this.navigation();
        // this.innerPulldown();
        this.changeHeaderDOM();
    }
}

var anime = {
    els: $('[data-inview]'),
    buffer: $(window).innerHeight / 5,

    inview: function() {
        const els = document.querySelectorAll('[data-view]')
        const buffer = window.innerHeight / 5

        function getElPosTop(el) {
            return el.getBoundingClientRect().top
        }
        function getElHeight(el) {
            return el.offsetHeight
        }
        
        function isInView(elPosTop, windowHeight, elHeight) {
            if (elPosTop < windowHeight - buffer) {
            return true
            } 
            return false
        }
        
        window.addEventListener('resize', function() {
            for(let i = 0; i < els.length; i++) {
                if (isInView(getElPosTop(els[i]), window.innerHeight, getElHeight(els[i]))) {
                    els[i].classList.add('js-view')
                }   
            }
        })
        
        window.addEventListener('scroll', function() {
            for(let i = 0; i < els.length; i++) {
                if (isInView(getElPosTop(els[i]), window.innerHeight, getElHeight(els[i]))) {
                    els[i].classList.add('js-view')
                } 
            }
        })
    },

    pricePage: function() {
        if(!$('.price').length) return;
        TweenMax.fromTo('.l-container', 1.5,
            { opacity: 0, y: 20 },
            { opacity: 1, y: 0, delay: .1, ease: Power3.easeOut }
        )
    },

    newsPage: function() {
        if(!$('.news').length) return;
        TweenMax.fromTo('.l-container', 1.5,
            { opacity: 0, y: 20 },
            { opacity: 1, y: 0, delay: .1, ease: Power3.easeOut }
        )
    },

    init: function() {
        this.inview();
        this.pricePage();
        this.newsPage();
    }
}


/* =====================================================================
    読み込み完了時の処理
 * ================================================================== */
document.addEventListener('DOMContentLoaded', function() {
    atags.smoothScroll();
    atags.languageChange();
    header.init();
    anime.init();
});
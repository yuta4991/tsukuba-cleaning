/*==============================================

    0. GULP SETTING

==============================================*/
var gulp = require('gulp');
var fs = require('fs');
var $ = require('gulp-load-plugins')();
var runSequence = require('run-sequence');
var browserSync = require('browser-sync').create();
var path = require('path');
var PATH_SRC = 'src';
var PATH_DEST = 'dest';

/*==============================================

    1. SCSS

==============================================*/
gulp.task('scss', () => {
    return gulp.src(PATH_SRC + '/asset/css/**/*.scss')
        .pipe($.changed(PATH_SRC))
        .pipe($.plumber({
            errorHandler: $.notify.onError("Error: <%= error.message %>")
        }))
        .pipe($.sass({
            indentType: 'tab',
            indentWidth: 1,
            outputStyle: 'expanded'
        }).on('error', $.sass.logError))
        .pipe($.autoprefixer({cascade: false}))
        .pipe(gulp.dest(PATH_DEST + '/asset/css'))
        .pipe(browserSync.stream());
});

/*==============================================

    2. EJS

==============================================*/
gulp.task('ejs', () => {
    var locals = {
		'sitedata' : JSON.parse(fs.readFileSync(PATH_SRC + '/sitedata.json'))
    };
    gulp.src([PATH_SRC + '/**/*.ejs', '!' + PATH_SRC + '/**/_*.ejs'])
        .pipe($.plumber({errorHandler: $.notify.onError("Error: <%= error.message %>")}))
        .pipe($.data(function(file){
			locals.relativePath = path.relative(file.base, file.path.replace(/.ejs$/, '.html'));
			return locals;
		}))
		.pipe($.ejs(locals.sitedata, {}, {ext: '.html'}))
		.pipe(gulp.dest(PATH_DEST));
});

/*==============================================

    3. PUG

==============================================*/
gulp.task('pug', () => {
    var locals = {
		'sitedata' : JSON.parse(fs.readFileSync(PATH_SRC + '/sitedata.json'))
    };
    return gulp.src([PATH_SRC + '/**/*.pug', '!' + PATH_SRC + '/**/_*.pug'])
        .pipe($.plumber({errorHandler: $.notify.onError("Error: <%= error.message %>")}))
        .pipe($.data(function(file){
			locals.relativePath = path.relative(file.base, file.path.replace(/.pug$/, '.html'));
            return locals;
		}))
        .pipe($.pug({
            locals: locals,
            basedir: 'src',
            pretty:'\t'
        }))
    .pipe($.changed(PATH_SRC))
    .pipe(gulp.dest(PATH_DEST))
    .pipe(browserSync.stream());
});

/*==============================================

    4. COPY

==============================================*/
gulp.task('copy', () => {
    return gulp.src([
        PATH_SRC + '/**/*.{html,php,js,pdf,mp4,woff,ttf,otf,svg,css,jpg,png}'
    ])
    .pipe(gulp.dest(PATH_DEST))
} );

/*==============================================

    5. LOCAL SERVER

==============================================*/
gulp.task('browser-sync', () => {
    browserSync.init({
        server: {
            baseDir: PATH_DEST,
            index  : "index.html",
            open:'external'
        }
    });
});

/*==============================================

    $. WATCH EVERY TIME

==============================================*/
// watch 毎回実行して欲しい処理
gulp.task('watch', () => {
    gulp.watch(PATH_SRC + '/**/*.scss', gulp.task('scss'));
    gulp.watch(PATH_SRC + '/**/*.pug', gulp.task('pug'));
    gulp.watch(PATH_SRC + '/**/*.ejs', gulp.task('ejs'));
    gulp.watch(PATH_SRC + '/**/*.{js,jpg,png,gif,svg}', gulp.task('copy'));
});

gulp.task('build', gulp.series('copy', 'pug', 'scss', 'browser-sync'));

gulp.task("default", gulp.parallel('build', 'watch'));
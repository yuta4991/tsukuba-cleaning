/**
 * Home JavaScript
 * 
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v3.1.1
 */
var homeJavaScript = {
    element_0: $('[data-loaded="0"]'),
    element_1: $('[data-loaded="1"]'),
    element_2: $('[data-loaded="2"]'),
    element_3: $('[data-loaded="3"]'),
    element_4: $('[data-loaded="4"]'),

    loaded: function() {
        TweenMax.fromTo(this.element_0, 1.5,
            { opacity: 0, y: 5 },
            { opacity: 1, y: 0, delay: .1, ease: Power3.easeOut }
        )
        TweenMax.fromTo(this.element_1, 1.2,
            { opacity: 0, y: 20 },
            { opacity: 1, y: 0, delay: .2, ease: Power3.easeOut }
        )
        TweenMax.fromTo(this.element_2, 1.2,
            { opacity: 0, y: 20 },
            { opacity: 1, y: 0, delay: .3, ease: Power3.easeOut }
        )
        TweenMax.fromTo(this.element_3, 1.2,
            { opacity: 0, y: 20 },
            { opacity: 1, y: 0, delay: .4, ease: Power3.easeOut }
        )
        TweenMax.fromTo(this.element_4, 1.2,
            { opacity: 0, y: 50 },
            { opacity: 1, y: 0,  delay: 1.2, ease: Power3.easeOut }
        )
    },

    init: function () {
        this.loaded();
    }
}

/**
 * Home News Ticker
 * 
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v3.1.1
 */
var news = {
    ticker: function() {
        var $setElm = $('.t-news__ticker');
        var $elmBox = $('.t-news__ticker__table tr');
		var effectSpeed = 1000;
		var switchDelay = 6000;
        var easing = 'swing';

		$setElm.each(function(){
			var effectFilter = $(this).attr('rel');

			var $targetObj = $(this);
			var $targetUl = $targetObj.children('table');
			var $targetLi = $targetObj.find('tr');
			var $setList = $targetObj.find('tr:first');

			var ulWidth = $targetUl.width();
			var listHeight = $targetLi.height();
			$targetObj.css({height:(listHeight)});
			$targetLi.css({top:'0',left:'0',position:'absolute'});

			if(effectFilter == 'roll') {
				$setList.css({top:'2em',display:'block',opacity:'0',zIndex:'98'}).stop().animate({top:'0',opacity:'1'},effectSpeed,easing).addClass('showlist');
				setInterval(function(){
					var $activeShow = $targetObj.find('.showlist');
					$activeShow.animate({top:'-2em',opacity:'0'},effectSpeed,easing).next().css({top:'3em',display:'block',opacity:'0',zIndex:'99'}).animate({top:'0',opacity:'1'},effectSpeed,easing).addClass('showlist').end().appendTo($targetUl).css({zIndex:'98'}).removeClass('showlist');
				},switchDelay);
			}
		});
    }
}


/* =====================================================================
    読み込み完了時の処理
 * ================================================================== */
document.addEventListener('DOMContentLoaded', function() {
    homeJavaScript.init();
    news.ticker();
    // $('.t-news__ticker__table tr').show();
});